'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    LogSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  case_id: {type: Schema.Types.ObjectId, ref: "CaseFile"},
  caseId: String,
  caseName: String,
  viewedBy: {type: Schema.Types.ObjectId, ref: "User"}
});

mongoose.model('Log', LogSchema);