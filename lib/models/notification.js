'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    NotificationSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  author: {type: Schema.Types.ObjectId, ref: "User"},
  caseFiles: [{type: Schema.Types.ObjectId, ref: "Case"}],
  owner: {type: Schema.Types.ObjectId, ref: "User"},
  expire: { type: Date },
  note: {type: String},  
});

mongoose.model('Notification', NotificationSchema);