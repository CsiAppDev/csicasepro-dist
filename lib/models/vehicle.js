'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

VehicleSchema = new Schema(

{
  created: { type: Date, default: Date.now }, 
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  owner: {
    id: {type: Schema.Types.ObjectId},
    schema: String
  },
  caseFile: {type: Schema.Types.ObjectId},
  make: String,
  model: String,
  year: Number,
  color: String,
  vin: String,
  removedBy: String,
  commercial: Boolean,
  carrierName: String,
  wrecker: {
    company: String,
    contact: String,
    phone: String
  },
  driver: {

    firstName: String,
    middleName: String,
    lastName: String,
    license: {
      number: String,
      state: String
    },
    address: String,
    city: String,
    state: String,
    zip: String,
    phones: [{label: Number, value: String}],
    citation: Boolean

  },
  passengers: [{type: Schema.Types.ObjectId, ref: "Passenger"}],
  tasks: [{type: Schema.Types.ObjectId, ref: "Inspection"}],
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}] 
 
});



mongoose.model('Vehicle', VehicleSchema);