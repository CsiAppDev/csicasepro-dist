'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    TaskSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  caseFile: {type: Schema.Types.ObjectId},
  type: String,
  label: String,
  owner: {
    id: {type: Schema.Types.ObjectId},
    schema: String
  },
  dateAssigned: { type: Date, default: Date.now },
  dateDue: { type: Date },
  dateTimePerformed: { type: Date },
  assignedBy: {type: Schema.Types.ObjectId, ref: "User"},
  investigators: [{type: Schema.Types.ObjectId, ref: "User"}],
  fields: [{type: Schema.Types.Mixed}],
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}]
});

mongoose.model('Task', TaskSchema);