'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema;

//mongoose.set('debug', true);
    
/**
 * Case Schema   , dropDups: true
 */
var OldCaseSchema = new Schema(
  {
    caseId:  { type: String, index: {unique: true}},
    trash: { type: Boolean, default: false },
    converted: { type: Date },
    location: String,
    crashNum: { type: String, index: true },
    crashDate: String,
    plantiffOrDefense: String,
    attorney: String,
    caseName: String,
    state: String,
    cityCounty: String,
    pedestrian: String,
    driver1Name: String,
    car1TypeOrCarrierName: String,
    passengers: String,
    passengers1: String,
    driver2Name: String,
    car2TypeOrCarrierName: String,
    passengers2: String,
    driver3Name: String,
    car3TypeOrCarrierName: String,
    passengers3: String,
    driver4Name: String,
    car4TypeOrCarrierName: String,
    passengers4: String,
    driver5Name: String,
    passengers5: String,
    car5TypeOrCarrierName: String,
    driver6Name: String,
    car6TypeOrCarrierName: String,
    passengers6: String,
    driver7Name: String,
    car7TypeOrCarrierName: String,
    passengers7: String,
    driver8Name: String,
    car8TypeOrCarrierName: String,
    passengers8: String,
    created: { type: Date, default: Date.now }, 
    updated: { type: Date, default: Date.now }
  }

/*{
  accidentReport: Boolean,
  caseId: String,
  caseName: String,
  crashDate: Date,
  crashNum: String,
  driverFirstName: String,
  driverLastName: String,
  fatalityTotalNum: Number,
  injuryTotalNum: Number,
  passengerName: String,
  plantiffOrDefense: String,
  updated: { type: Date, default: Date.now },
  vehiclesTotalNum: Number
  }

  */);

// Public name information
OldCaseSchema
  .virtual('recent')
  .get(function() {
    return this.caseId + " " + this.caseName;
  });


  // Public details information
OldCaseSchema
  .virtual('details')
  .get(function() {
    return this;
  });

OldCaseSchema.plugin(uniqueValidator,  { message: 'Value is not unique.' });
/**
 * Pre-save hook
 */
OldCaseSchema
  .pre('save', function(next) {
    next();
  });    

/**
 * Validations
 */
//r validatePresenceOf = function(value) {
  //turn value && value.length;
//

/**
 * Validations
 */
 /*
CaseSchema.path('crashNum').validate(function (num) {
  return num >= 1 && num <= 10;
}, 'Crash Number must be between 1 and 10');
*/


mongoose.model('OldCaseFile', OldCaseSchema);
