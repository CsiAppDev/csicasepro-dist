'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    PedestrianSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  caseFile: {type: Schema.Types.ObjectId},
  title: String,
  firstName: String,
  middleName: String,
  lastName: String,
  suffix: String,
  emails: [{label: Number, value: String}],
  phones: [{label: Number, value: String}],
  address: String,
  city: String,
  state: String,
  zip: String,
  attorney: {type: Schema.Types.ObjectId, ref: "Attorney"},
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}]  
});

mongoose.model('Pedestrian', PedestrianSchema);