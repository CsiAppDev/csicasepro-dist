'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    NoteSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  caseFile: {type: Schema.Types.ObjectId},
  owner: {
    id: {type: Schema.Types.ObjectId},
    schema: String
  },
  author: {type: Schema.Types.ObjectId, ref: "User"},
  note: {type: String},  
});

mongoose.model('Note', NoteSchema);