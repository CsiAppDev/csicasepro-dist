'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,

    DriverSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  title: String,
  firstName: String,
  middleName: String,
  lastName: String,
  suffix: String,
  email: [String],
  phone: [String],
  address: String,
  city: String,
  state: String,
  zip: String,
  caseFiles: [{type: Schema.Types.ObjectId, ref: "Case"}],
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}], 
  citationIssued: Boolean
});

mongoose.model('Driver', DriverSchema);