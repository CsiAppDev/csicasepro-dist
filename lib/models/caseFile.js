'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,

  
/**
 * Case Schema   , dropDups: true
 */
  CaseFileSchema = new Schema(

{
  created: { type: Date, default: Date.now },
  author: {type: Schema.Types.ObjectId, ref: "User"},
  currentUser: {type: Schema.Types.ObjectId, ref: "User"},
  trash: {type: Boolean, default: false },
  caseId:  { type: String, index: {unique: true}}, 
  tags: [String],
  generalInfo: {
    caseName: { type: String },
    crashNum: { type: String },
    injuries: {type: Number},
    fatalities: {type: Number},
    totalVehicles: {type: Number},
    noAccidentReport: {type: Boolean},
    clientType: String,
    attorney: {type: Schema.Types.ObjectId, ref: "Attorney"},
    isCriminal: Boolean
  },
  crashInfo: {
    reportingAgency: { type: String },
    officerName: { type: String },
    crashDateTime: { type: Date },
    road: { type: String },
    intersection: { type: String },
    city: { type: String },
    county: { type: String },
    state: { type: String },
    cityLimits: Boolean,

  },
  participants: {
    vehicles: [{type: Schema.Types.ObjectId, ref: "Vehicles"}],
    vehicleSummary: [{type: Schema.Types.Mixed}],
    pedestrians: [{type: Schema.Types.ObjectId, ref: "Pedestrians"}],
    pedestrianSummary: [{type: Schema.Types.Mixed}]
  },
  billing: {
    billAttorney: Boolean,
    firmName: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    address1: { type: String },
    address2: { type: String },
    city: { type: String },
    state: { type: String },
    zip: { type: String },
    emails: [{label: Number, value: { type: String }}],
    phones: [{label: Number, value: { type: String }}],
  },
  tasks: [{type: Schema.Types.ObjectId, ref: "Task"}],
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}]


});



CaseFileSchema.plugin(uniqueValidator,  { message: 'Value is not unique.' });
/**
 * Pre-save hook
 */
 /*
CaseFileSchema
  .pre('save', function(next) {
    next();
  });    
*/
/**
 * Validations
 */
//r validatePresenceOf = function(value) {
  //turn value && value.length;
//

/**
 * Validations
 */
 /*
CaseFileSchema.path('crashNum').validate(function (num) {
  return num >= 1 && num <= 10;
}, 'Crash Number must be between 1 and 10');
*/

//mongoose.set('debug', true);
mongoose.model('CaseFile', CaseFileSchema);
