'use strict';
//This is unused.  Now a part of Tasks
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

RecordRequestSchema = new Schema(

{
  created: { type: Date, default: Date.now }, 
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  caseFile: {type: Schema.Types.ObjectId},
  title: String,
  itemsRequested: [String],
  agency: {type: Schema.Types.ObjectId, ref: "Agency"},
  attorney: [{type: Schema.Types.ObjectId, ref: "Attorney"}],
  driver: {type: Schema.Types.ObjectId, ref: "Driver"},
  passengers: [{type: Schema.Types.ObjectId, ref: "Passenger"}],
  inspections: [{type: Schema.Types.ObjectId, ref: "Inspection"}],
  caseFiles: [{type: Schema.Types.ObjectId, ref: "Case"}],
  notes: [{type: Schema.Types.ObjectId, ref: "Note"}] 
 
});



mongoose.model('RecordRequest', RecordRequestSchema);