'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,


AgencySchema = new Schema(

{
  created: { type: Date, default: Date.now }, 
  updated: { type: Date, default: Date.now },
  trash: {type: Boolean, default: false },
  firstName: String,
  lastName: String,
  label: String,
  emails: [{label: Number, value: String}],
  phones: [{label: Number, value: String}],
  address1: String,
  address2: String,
  city: String,
  state: String,
  zip: String
  
});



mongoose.model('Agency', AgencySchema);