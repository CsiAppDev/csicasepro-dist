'use strict';

/**
   * Set a cookie for angular so it knows we have an http session
   */
var setUserCookie = function(req, res) {
    if(req.user) { //req.user
      res.cookie('user', JSON.stringify(req.user.userInfo), { expires : false });
    }
};

/**
 * Custom middleware used by the application
 */
module.exports = {

  /**
   *  Protect routes on your api from unauthenticated access
   */
  auth: function auth(req, res, next) {
    

    //next(); //remove to secure 

    if (req.isAuthenticated()) { 

      setUserCookie(req, res);

      return next();
      
    } else {
      res.redirect(302, '/login');
      
    }

    
  }
};