'use strict';

var mongoose = require('mongoose'),
    Notification = mongoose.model('Notification');

/**
 * Create Notification
 */
exports.create = function (req, res, next) {
  var newNotification = new Notification(req.body);
  newNotification.provider = 'local';

  newNotification.save(function(err) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save NotificationFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  Notification.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete NotificationFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Notification.findOne(objectId, function (err, Notification) {
    Notification.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Notifications
 */
exports.query = function(req, res) {

  var params = {};

  return Notification.find(params, function (err, Notifications) {
    if (!err) {
      return res.json(Notifications);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified Notification
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Notification.findOne({"_id": _id}, function (err, Notification) {
    if (err) return next(new Error('Failed to load Notification'));
  
    if (Notification) {
      res.send({ Notification: Notification });
    } else {
      res.send(404, 'Notification_NOT_FOUND');
    }
  });
};

