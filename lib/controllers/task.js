'use strict';

var mongoose = require('mongoose'),
    Task = mongoose.model('Task'),
    Vehicle = mongoose.model('Vehicle'),
    CaseFile = mongoose.model('CaseFile'),
    Search = require('./search'),
    Schemas = {"Vehicle": Vehicle, "CaseFile": CaseFile};

/**
 * Create Task
 */
exports.create = function (req, res, next) {
  var newTask = new Task(req.body),
      owner = req.body.owner;

  newTask.provider = 'local';

  newTask.save(function(err, task) {
    if (err) {
      return res.json(400, err);
    }

    Search.indexEntity('task', task);

    /*
    Iterate through the investigators list, put the current task ID into the user's task array (new). 

    Through this process we can send out notifications.  Say in the user controller look for updates that are task assignments

    or maybe exprt a new method that calls update... something like addTask, deleteTask, and when tasks come in, send out a notification?

    will also need a notification sent from the Task controller to affilliated parties.  Perhaps that's where the notifications should come from, 
    not from the user controller. 


    */

    if (owner) {
      Schemas[owner.schema].update({_id: mongoose.Types.ObjectId(owner.id)}, 
        { $push: { tasks: task.id } },
        {multi: false},
        function(err, numberAffected, rawResponse) {
            if (err) {
                res.send(500, err);
              } else {
                res.send(200, task);
              }
            console.log("any saving errors", err, numberAffected, rawResponse);
        });


    } else {
      return res.json(task);
    }

    

  });

  
  
};

/**
 * save TaskFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      _id = req.body._id;

  delete req.body._id;

  Task.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            req.body._id = _id;
            Search.indexEntity('task', req.body);
            res.send(200, req.body);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete TaskFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Task.findOne(objectId, function (err, task) {
    var owner = task._doc.owner;

    if (owner) {
        Schemas[owner.schema].update({_id: mongoose.Types.ObjectId(owner.id)}, 
          { $pull: { tasks: task.id } },
          {multi: false},
          function(err, numberAffected, rawResponse) {
              if (err) {
                  res.send(500, err);
                } else {
                  res.send(200);
                }
              console.log("any saving errors", err, numberAffected, rawResponse);
          });
    }

    Search.removeEntity('task', task);

    task.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          return res.send(200);
        }

    });
  });
};


/**
 *  Get all Tasks
 */
exports.query = function(req, res) {

  var query = req.query,
      taskIds = [];

  if (query && query.owner) {
    Schemas[query.type].findOne({"_id": query.owner}, function (err, item) {
          
      if (item && item.tasks) {
        taskIds = item.tasks;
      } 

      return Task.find({'_id': { $in: taskIds}}, function (err, Tasks) {
        if (!err) {
          return res.json(Tasks);
        } else {
          return res.send(err);
        }
      });

    });
  }

};

/**
 *  Get specified Task
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Task.findOne({"_id": _id}, function (err, Task) {
    if (err) return next(new Error('Failed to load Task'));
  
    if (Task) {
      res.send({ Task: Task });
    } else {
      res.send(404, 'Task_NOT_FOUND');
    }
  });
};

