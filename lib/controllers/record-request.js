'use strict';

var mongoose = require('mongoose'),
    RecordRequest = mongoose.model('RecordRequest');

/**
 * Create RecordRequest
 */
exports.create = function (req, res, next) {
  var newRecordRequest = new RecordRequest(req.body);
  newRecordRequest.provider = 'local';

  newRecordRequest.save(function(err) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save RecordRequestFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  RecordRequest.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete RecordRequestFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   RecordRequest.findOne(objectId, function (err, RecordRequest) {
    RecordRequest.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all RecordRequests
 */
exports.query = function(req, res) {

  var params = {};

  return RecordRequest.find(params, function (err, RecordRequests) {
    if (!err) {
      return res.json(RecordRequests);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified RecordRequest
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  RecordRequest.findOne({"_id": _id}, function (err, RecordRequest) {
    if (err) return next(new Error('Failed to load RecordRequest'));
  
    if (RecordRequest) {
      res.send({ RecordRequest: RecordRequest });
    } else {
      res.send(404, 'RecordRequest_NOT_FOUND');
    }
  });
};

