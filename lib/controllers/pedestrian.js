'use strict';

var mongoose = require('mongoose'),
    CaseFile = mongoose.model('CaseFile'),
    Pedestrian = mongoose.model('Pedestrian'),
    Search = require('./search');

/**
 * Create Pedestrian
 */
exports.create = function (req, res, next) {
  var newPedestrian = new Pedestrian(req.body),
      owner = req.body.owner;

  newPedestrian.provider = 'local';

  newPedestrian.save(function(err, pedestrian) {
    if (err) {

      return res.json(400, err);
    }

    Search.indexEntity('pedestrian', pedestrian);

    if (owner) {
      CaseFile.update({_id: mongoose.Types.ObjectId(owner.id)}, 
        { $push: { "participants.pedestrians": pedestrian.id } },
        {multi: false},
        function(err, numberAffected, rawResponse) {
            if (err) {
                res.send(500, err);
              } else {
                res.send(200, pedestrian);
              }
            console.log("any saving errors", err, numberAffected, rawResponse);
        });

    } else {
      return res.send(200, pedestrian);
    }

  });
};

/**
 * save Pedestrian File
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      _id = req.body._id;

  delete req.body._id;

  Pedestrian.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            req.body._id = _id;
            Search.indexEntity('pedestrian', req.body);
            res.send(200, req.body);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete PedestrianFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Pedestrian.findOne(objectId, function (err, pedestrian) {

    Search.removeEntity('pedestrian', pedestrian);

    pedestrian.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};



exports.query = function(req, res) {

  var query = req.query,
      pedestriansIds = [];

  if (query && query.owner) {
    CaseFile.findOne({"_id": query.owner}, function (err, item) {
          
      if (item && item.participants.pedestrians) {
        pedestriansIds = item.participants.pedestrians;
      } 

      return Pedestrian.find({'_id': { $in: pedestriansIds}}, function (err, pedestrians) {
        if (!err) {
          return res.json(pedestrians);
        } else {
          return res.send(err);
        }
      });

    });
  }


  
};



/**
 *  Get specified Pedestrian
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Pedestrian.findOne({"_id": _id}, function (err, pedestrian) {
    if (err) return next(new Error('Failed to load Pedestrian'));
  
    if (pedestrian) {
      res.send({ Pedestrian: pedestrian });
    } else {
      res.send(404, 'Pedestrian_NOT_FOUND');
    }
  });
};

