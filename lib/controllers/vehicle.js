'use strict';

var mongoose = require('mongoose'),
    CaseFile = mongoose.model('CaseFile'),
    Vehicle = mongoose.model('Vehicle'),
    Search = require('./search');

/**
 * Create Vehicle
 */
exports.create = function (req, res, next) {
  var newVehicle = new Vehicle(req.body),
      owner = req.body.owner;

  newVehicle.provider = 'local';

  newVehicle.save(function(err, vehicle) {
    if (err) {

      return res.json(400, err);
    }

    Search.indexEntity('vehicle', vehicle);

    if (owner) {
      CaseFile.update({_id: mongoose.Types.ObjectId(owner.id)}, 
        { $push: { "participants.vehicles": vehicle.id } },
        {multi: false},
        function(err, numberAffected, rawResponse) {
            if (err) {
                res.send(500, err);
              } else {
                res.send(200, vehicle);
              }
            console.log("any saving errors", err, numberAffected, rawResponse);
        });

    } else {
      return res.send(200, vehicle);
    }

  });
};

/**
 * save VehicleFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      _id = req.body._id;

  delete req.body._id;

  Vehicle.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            req.body._id = _id;
            Search.indexEntity('vehicle', req.body);
            res.send(200, req.body);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete VehicleFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Vehicle.findOne(objectId, function (err, Vehicle) {

    Search.removeEntity('vehicle', vehicle);

    Vehicle.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};



exports.query = function(req, res) {

  var query = req.query,
      vehiclesIds = [];

  if (query && query.owner) {
    CaseFile.findOne({"_id": query.owner}, function (err, item) {
          
      if (item && item.participants.vehicles) {
        vehiclesIds = item.participants.vehicles;
      } 

      return Vehicle.find({'_id': { $in: vehiclesIds}}, function (err, vehicles) {
        if (!err) {
          return res.json(vehicles);
        } else {
          return res.send(err);
        }
      });

    });
  }


  
};



/**
 *  Get specified Vehicle
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Vehicle.findOne({"_id": _id}, function (err, Vehicle) {
    if (err) return next(new Error('Failed to load Vehicle'));
  
    if (Vehicle) {
      res.send({ Vehicle: Vehicle });
    } else {
      res.send(404, 'Vehicle_NOT_FOUND');
    }
  });
};

