'use strict';

var mongoose = require('mongoose'),
    elasticsearch = require('elasticsearch'),
    Vehicle = mongoose.model('Vehicle'),
    Passenger = mongoose.model('Passenger'),
    Pedestrian = mongoose.model('Pedestrian'),
    Note = mongoose.model('Note'),
    Task = mongoose.model('Task'),
    CaseFile = mongoose.model('CaseFile'),
    Schemas = {
        "vehicle": Vehicle,
        "casefile": CaseFile,
        "note": Note,
        "task": Task,
        "passenger": Passenger
    },

    Client = new elasticsearch.Client({
      host: 'https://paas:0fefac826d6588ae0460318faca085ed@dwalin-us-east-1.searchly.com',//
      log: 'trace'
    }),

    indexName = 'csindex1',

    getEntities = function(results, res) {
        var entities = [],
            hits = results.hits.hits,
            remaining = hits.length,
            finishRequest = function() {
                remaining -= 1;
                if (remaining <= 0) {
                    results.hits.entities = entities;
                    res.send(200, results);
                }
            };

        while(hits.length >0) {
            var entity = hits.pop();
            Schemas[entity._type].findOne({"_id": entity._id}, 
                function (err, item) {

                    if (err) return next(new Error('Failed to load item...'));
                    
                    entities.push(item.toObject());

                    finishRequest();
                }    
            );
        }



    };


exports.search = function(req, res) {
    var query = req.query;

    Client.search({
      index: indexName,
      q: query.search
      //, fields: "'_id'"
    }, function (error, response) {
       if (error) {
          res.send(400, error);
        } else {
          //getEntities(response, res)
          res.send(200, response);
        }
    });

};

exports.indexEntity = function(type, entity) {

    Client.index({
            index: indexName,
            type: type,
            id: entity._id.toString(),
            body: entity
          }, function (error, response) {
              console.log("indexed: (err/response)", error, response);
    });
};


exports.removeEntity = function(type, entity) {

    Client.delete({
            index: indexName,
            type: type,
            id: entity._id.toString()

          }, function (error, response) {
              console.log("removed entity: (err/response)", error, response);
    });
};