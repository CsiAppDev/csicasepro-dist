'use strict';

var mongoose = require('mongoose'),
    Agency = mongoose.model('Agency');

/**
 * Create Agency
 */
exports.create = function (req, res, next) {
  var newAgency = new Agency(req.body);
  newAgency.provider = 'local';

  newAgency.save(function(err) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save Agency
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  Agency.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete Agency
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Agency.findOne(objectId, function (err, Agency) {
    Agency.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Agencies
 */
exports.query = function(req, res) {

  var params = {};

  return Agency.find(params, function (err, Agencys) {
    if (!err) {
      return res.json(Agencys);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified Agency
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Agency.findOne({"_id": _id}, function (err, Agency) {
    if (err) return next(new Error('Failed to load Agency'));
  
    if (Agency) {
      res.send({ Agency: Agency });
    } else {
      res.send(404, 'Agency_NOT_FOUND');
    }
  });
};

