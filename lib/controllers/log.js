'use strict';

var mongoose = require('mongoose'),
    Log = mongoose.model('Log');

/**
 * Create Log
 */
exports.create = function (req, res, next) {
  var newLog = new Log(req.body);
  newLog.provider = 'local';

  newLog.save(function(err) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save LogFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  Log.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete LogFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Log.findOne(objectId, function (err, Log) {
    Log.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Logs
 */
exports.query = function(req, res) {

  var params = {};

  return Log.find(params).sort({"created": -1}).limit(5).exec(function (err, Logs) {
    if (!err) {
      return res.json(Logs);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified Log
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Log.findOne({"_id": _id}, function (err, Log) {
    if (err) return next(new Error('Failed to load Log'));
  
    if (Log) {
      res.send({ Log: Log });
    } else {
      res.send(404, 'Log_NOT_FOUND');
    }
  });
};

