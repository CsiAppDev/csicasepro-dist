'use strict';

var mongoose = require('mongoose'),
    Driver = mongoose.model('Driver');

/**
 * Create Driver
 */
exports.create = function (req, res, next) {
  var newDriver = new Driver(req.body);
  newDriver.provider = 'local';

  newDriver.save(function(err) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save DriverFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  Driver.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete DriverFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Driver.findOne(objectId, function (err, Driver) {
    Driver.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Drivers
 */
exports.query = function(req, res) {

  var params = {};

  return Driver.find(params, function (err, Drivers) {
    if (!err) {
      return res.json(Drivers);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified Driver
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Driver.findOne({"_id": _id}, function (err, Driver) {
    if (err) return next(new Error('Failed to load Driver'));
  
    if (Driver) {
      res.send({ Driver: Driver });
    } else {
      res.send(404, 'Driver_NOT_FOUND');
    }
  });
};

