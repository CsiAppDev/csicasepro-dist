'use strict';

var mongoose = require('mongoose'),
    Attorney = mongoose.model('Attorney');

/**
 * Create Attorney
 */
exports.create = function (req, res, next) {
  var newAttorney = new Attorney(req.body);
  newAttorney.provider = 'local';

  newAttorney.save(function(err, attorney) {
    if (err) {

      return res.json(400, err);
    }

    return res.json(attorney);
  });
};

/**
 * save AttorneyFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id);

  delete req.body._id;

  Attorney.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete AttorneyFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Attorney.findOne(objectId, function (err, attorney) {
    attorney.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Attorneys
 */
exports.query = function(req, res) {

  var params = {};

  return Attorney.find(params, function (err, attorneys) {
    if (!err) {
      return res.json(attorneys);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified Attorney
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Attorney.findOne({"_id": _id}, function (err, attorney) {
    if (err) return next(new Error('Failed to load Attorney'));
  
    if (attorney) {
      res.send({ Attorney: attorney });
    } else {
      res.send(404, 'Attorney_NOT_FOUND');
    }
  });
};

