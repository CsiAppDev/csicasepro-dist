'use strict';

var mongoose = require('mongoose'),
    CaseFile = mongoose.model('CaseFile'),
    Log = mongoose.model('Log'),
    elasticsearch = require('elasticsearch'),
    passport = require('passport'),
    Search = require('./search');


/**
 * Create Case
 */
exports.create = function (req, res, next) {
  var newCase = new CaseFile(req.body);
  newCase.provider = 'local';

  newCase.save(function(err, caseFile) {
    if (err) {
      // Manually provide our own message for 'unique' validation errors, can't do it from schema
      if(err.errors && err.errors.caseId.type === 'user defined') { // was 'Value is not unique.'
        err.errors.caseId.type = 'The specified CSI case id is already in use.';
      }
      return res.json(400, err);
    }

    Search.indexEntity('casefile', caseFile);

    return res.send(200, caseFile);
  });
};

/**
 * save caseFile
 */


exports.saveFile = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      caseFile = req.body,
       _id = caseFile._id;

  delete caseFile._id;
  
  CaseFile.update({_id: objectId}, 
    {$set: caseFile }, 
    {multi: false},
    function(err, numberAffected, rawResponse, whatelse) {
        if (err) {
            res.send(500, err);
          } else {
            caseFile._id = _id;
            Search.indexEntity('casefile', caseFile);
            res.send(200, caseFile);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });


};


/**
 * delete caseFile
 */

exports.removeCaseFile = function(req, res, next) {
 
   CaseFile.findOne({_id: mongoose.Types.ObjectId(req.params.id)}, function (err, caseFile) {

    Search.removeEntity('casefile', caseFile);

    caseFile.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });

  });
};


/**
 *  Get paginated/sorted cases
 */
exports.getCases = function(req, res) {

  var query = req.query,
      hideTrash = { $or: [ {"trash": {"$exists": false} }, {"trash": false}]},
      showTrash = {"trash": true},
      sortBy = {},
      params = req.query.showAll ? showTrash : hideTrash;

      sortBy[query.sortBy.replace('_', '.')] = query.direction;

  CaseFile.find(params)
          .sort(sortBy)
          .skip(query.startIndex)
          .limit(query.pageSize)
          .exec(function (err, casez) {
            if (!err) {
              return res.json(casez);
            } else {
              return res.send(err);
            }
        });

};


exports.getCaseCount = function(req, res) {

  var hideTrash = { $or: [ {"trash": {"$exists": false} }, {"trash": false}]};

  CaseFile.count(hideTrash, function (err, count) {
      if (!err) {
        var out = {count: count.toString()};
        return res.json(out);
      } else {
        return res.send(err);
      }
    });

};


exports.queryCases = function(req, res) {

  var hideTrash = { $or: [ {"trash": {"$exists": false} }, {"trash": false}]},
      showTrash = {"trash": true},
      params = req.query.showAll ? showTrash : hideTrash;


  CaseFile.find(params, function (err, casez) {
    if (!err) {
      return res.json(casez);
    } else {
      return res.send(err);
    }
  });

};



/**
 *  Get specified case
 */
exports.showCase = function (req, res, next) {
  var _id = req.params.id;

  CaseFile.findOne({"_id": _id}, function (err, caseFile) {
    if (err) return next(new Error('Failed to load Case'));
  
    if (caseFile) {
      Log.create({
        case_id: _id,
        caseId: caseFile.caseId,
        caseName: caseFile.generalInfo.caseName,
        viewedBy: caseFile.currentUser
      });
      res.send(200, { caseFile: caseFile });
    } else {
      res.send(404, 'CASE_NOT_FOUND');
    }
  });
};
