'use strict';

var mongoose = require('mongoose'),
    Vehicle = mongoose.model('Vehicle'),
    Search = require('./search'),
    Passenger = mongoose.model('Passenger');

/**
 * Create Passenger
 */
exports.create = function (req, res, next) {
  var newPassenger = new Passenger(req.body),
    owner = req.body.owner;

  newPassenger.provider = 'local';

  newPassenger.save(function(err, passenger) {
    if (err) {

      return res.json(400, err);
    }

    Search.indexEntity('passenger', passenger);

    if (owner) {
      Vehicle.update({_id: mongoose.Types.ObjectId(owner.id)}, 
        { $push: { "passengers": passenger.id } },
        {multi: false},
        function(err, numberAffected, rawResponse) {
            if (err) {
                res.send(500, err);
              } else {
                res.send(200, passenger);
              }
            console.log("any saving errors", err, numberAffected, rawResponse);
        });

    } else {
      return res.send(200, passenger);
    }
   
  });
};

/**
 * save PassengerFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      _id = req.body._id;

  delete req.body._id;

  Passenger.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            req.body._id = _id;
            Search.indexEntity('passenger', req.body);
            res.send(200, req.body);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete PassengerFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Passenger.findOne(objectId, function (err, Passenger) {

    Search.removeEntity('passenger', Passenger);

    Passenger.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all Passengers in a vehicle
 */
exports.query = function(req, res) {

    var query = req.query;

    if (query && query.owner) {
        Vehicle.findOne({"_id": query.owner}, function (err, vehicle) {

            return Passenger.find({'_id': { $in: vehicle.passengers}}, function (err, passengers) {
                        if (!err) {
                          return res.json(passengers);
                        } else {
                          return res.send(err);
                        }
                    });

        });
    }
};

/**
 *  Get specified Passenger
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Passenger.findOne({"_id": _id}, function (err, Passenger) {
    if (err) return next(new Error('Failed to load Passenger'));
  
    if (Passenger) {
      res.send({ Passenger: Passenger });
    } else {
      res.send(404, 'Passenger_NOT_FOUND');
    }
  });
};

