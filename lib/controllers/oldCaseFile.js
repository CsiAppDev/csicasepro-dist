'use strict';

var mongoose = require('mongoose'),
    OldCaseFile = mongoose.model('OldCaseFile'),
    passport = require('passport');

/**
 * Create Case
 */
exports.create = function (req, res, next) {
  var newCase = new OldCaseFile(req.body);
  newCase.provider = 'local';

  newCase.save(function(err, casefile) {
    if (err) {
      // Manually provide our own message for 'unique' validation errors, can't do it from schema
      if(err.errors.caseId.type === 'user defined') { // was 'Value is not unique.'
        err.errors.caseId.type = 'The specified CSI case id is already in use.';
      }
      return res.json(400, err);
    }

    return res.json(req.body);
  });
};

/**
 * save caseFile
 */


exports.saveFile = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      caseFile = req.body;

  delete caseFile._id;
  
  OldCaseFile.update({_id: objectId}, 
    {$set: caseFile }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            res.send(200);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });




};


/**
 * delete caseFile
 */

exports.removeOldCaseFile = function(req, res, next) {
  var _id = req.params._id;
  
   OldCaseFile.findOne(_id, function (err, caseFile) {
    caseFile.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          res.send(200);
        }
      });
  });
};


/**
 *  Get all cases
 */
exports.queryCases = function(req, res) {

  var hideTrash = { 
      $or: [ 
        {"trash": {"$exists": false} }, {"trash": false},
        {"converted": {"$exists": false} }, {"converted": null}
      ]

    },
      showTrash = {"trash": true},
      params = req.query.showAll ? showTrash : hideTrash;

      if (req.query.caseId) {
        params.caseId = req.query.caseId;
      }

  return OldCaseFile.find(params, function (err, casez) {
    if (!err) {
      return res.json(casez);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Get specified case
 */
exports.showCase = function (req, res, next) {
  var _id = req.params.id;

  OldCaseFile.findOne({"_id": _id}, function (err, caseFile) {
    if (err) return next(new Error('Failed to load Case'));
  
    if (caseFile) {
      
      res.send({ caseFile: caseFile.details });
    } else {
      res.send(404, 'CASE_NOT_FOUND');
    }
  });
};
