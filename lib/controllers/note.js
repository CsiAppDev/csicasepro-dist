'use strict';

var mongoose = require('mongoose'),
    Note = mongoose.model('Note'),
    Task = mongoose.model('Task'),
    Vehicle = mongoose.model('Vehicle'),
    CaseFile = mongoose.model('CaseFile'),
    Search = require('./search'),
    Schemas = {"Vehicle": Vehicle, "CaseFile": CaseFile, "Task": Task};

/**
 * Create Note
 */
exports.create = function (req, res, next) {
  var newNote = new Note(req.body),
      owner = req.body.owner;

  newNote.provider = 'local';

  newNote.save(function(err, note) {
    if (err) {
      return res.json(400, err);
    }

    Search.indexEntity('note', note);
    
    if (owner) {
      Schemas[owner.schema].update({_id: mongoose.Types.ObjectId(owner.id)}, 
        { $push: { notes: note.id } },
        {multi: false},
        function(err, numberAffected, rawResponse) {
            if (err) {
                res.send(500, err);
              } else {
                res.send(200, note);
              }
            console.log("any saving errors", err, numberAffected, rawResponse);
        });


    } else {
      return res.json(note);
    }

  });
};

/**
 * save NoteFile
 */


exports.save = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.body._id),
      _id = req.body._id;

  delete req.body._id;

  Note.update({_id: objectId}, 
    {$set: req.body }, 
    {multi: false},
    function(err, numberAffected, rawResponse) {
        if (err) {
            res.send(500, err);
          } else {
            req.body._id = _id;
            Search.indexEntity('note', req.body);
            res.send(200, req.body);
          }
        console.log("any saving errors", err, numberAffected, rawResponse);
    });

};


/**
 * delete NoteFile
 */

exports.remove = function(req, res, next) {
  var objectId = mongoose.Types.ObjectId(req.params.id);

   Note.findOne(objectId, function (err, note) {
    var owner = req.body.owner;

    if (owner) {
        Schemas[owner.schema].update({_id: mongoose.Types.ObjectId(owner.id)}, 
          { $pull: { notes: note.id } },
          {multi: false},
          function(err, numberAffected, rawResponse) {
              if (err) {
                  res.send(500, err);
                } else {
                  res.send(200);
                }
              console.log("any saving errors", err, numberAffected, rawResponse);
          });
    }

    Search.removeEntity('note', note);

    note.remove(function(err) {
        if (err) {
          res.send(500, err);
        } else {
          return res.send(200);
        }

      });
  });
};


/**
 *  Get notes
 */
exports.query = function(req, res) {

  var query = req.query,
      noteIds = [];

  if (query && query.owner) {
    Schemas[query.type].findOne({"_id": query.owner}, function (err, item) {
          
      if (item && item.notes) {
        
        noteIds = item.notes;
      } 

      return Note.find({'_id': { $in: noteIds}}, function (err, Notes) {
        if (!err) {
          return res.json(Notes);
        } else {
          return res.send(err);
        }
      });

    });
  }


  
};

/**
 *  Get specified Note
 */
exports.show = function (req, res, next) {
  var _id = req.params.id;

  Note.findOne({"_id": _id}, function (err, Note) {
    if (err) return next(new Error('Failed to load Note'));
  
    if (Note) {
      res.send({ Note: Note });
    } else {
      res.send(404, 'Note_NOT_FOUND');
    }
  });
};

