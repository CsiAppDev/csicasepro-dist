'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  User = mongoose.model('User'),
  CaseFile = mongoose.model('CaseFile'),
  Attorney = mongoose.model('Attorney'),
  Vehicle = mongoose.model('Vehicle'),
  Passenger = mongoose.model('Passenger'),
  Note = mongoose.model('Note'),
// dummy notes should no longer be used as there are saving issues. crud notes from ui.
  notes = [
    {
      "author": '535f4b49e9d28af855cc62b1',
      "caseFile": '535f4b4ae9d28af855cc62b3',
      "note": 'Here is a little story about a man named Jed; a poor mountaineer, barely kept his family fed.'
    },
    {
      "author": '535f4b4ae9d28af855cc62b2',
      "caseFile": '535f4b4ae9d28af855cc62b3',
      "note": 'One day he was shooting at some food when up from the ground came a bubbling crude.'
    }

  ],

  passengers = [
    {
      title: 'Mr',
      firstName: 'Ho',
      middleName: 'Lee',
      lastName: 'Fuk',
      suffix: 'Jr',
      emails: [
        {label: '0', value:'mark@williamstarker.com'}
        ],
      phones: [
        {label: '0', value: '404-555-4564'},
        {label: '2', value:'404-123-1234'},
        {label: '1', value: '678-545-4564'}
      ],
      address: '123 Main St',
      city: 'Oshkosh',
      state: 'WI',
      zip: '46546',
    },

    {
      title: 'Mrs',
      firstName: 'Sum',
      middleName: 'Doo',
      lastName: 'No',
      suffix: 'PHD',
      emails: [
        {label: '0', value:'sum@poochville.com'}
        ],
      phones: [
        {label: '0', value: '404-555-4564'},
        {label: '2', value:'404-123-1234'},
        {label: '1', value: '678-545-4564'}
      ],
      address: '333 New York St',
      city: 'Birmingham',
      state: 'MI',
      zip: '44873',
    }

  ],
  vehicles = [
    {
      make: 'Dodge',
      model: 'Stratus',
      year: 2005,
      color: 'Blue',
      vin: 'vin32165465456654',
      removedBy: 'driven',
      wrecker: {
        company: '',
        contact: '',
        phone: ''
      },
      commercial: false,
      carrierName: '',
      driver: {

        firstName: 'Warren',
        middleName: 'G',
        lastName: 'Harding',
        license: {
          number: '123456564',
          state: 'GA'
        },
        address: '123 Main St',
        city: 'Oshkosh',
        state: 'WI',
        zip: '46546',
        phones: [
          {label: '2', value:'404-123-1234'},
          {label: '1', value: '678-545-4564'}
        ],
        citation: true

      },
      passengers: ['535c1616708fa89c208fcf65', '535c1616708fa89c208fcf66']
      
    },
    {
      make: 'Nissan',
      model: 'Maxima',
      year: 2012,
      color: 'Lime',
      vin: 'vin123123156652',
      removedBy: 'wrecker',
      wrecker: {
        company: 'BigJohns',
        contact: 'Bill',
        phone: '205-254-9865'
      },
      commercial: false,
      carrierName: '',
      driver: {
        firstName: 'Timmothy',
        middleName: 'A',
        lastName: 'Leary',
        license: {
          number: '22244455',
          state: 'GA'
        },
        address: '545 Tiberious St',
        city: 'Macon',
        state: 'GA',
        zip: '32133',
        phones: [
          {label: '0', value:'404-555-4564'},
          {label: '1', value:'404-123-1234'}
        ],
        citation: false

      },
      passengers: ['535c1616708fa89c208fcf66', '535c1616708fa89c208fcf65']
    }

  ],


  attorneys = [{
    title: 'Mr.',
    firstName: 'John',
    middleName: 'C',
    lastName: 'Riley',
    suffix: '',
    firmName: 'Riley and Riley',
    emails: [
      {label: '0', value:'john@riley.com'}
      ],
    phones: [
      {label: '0', value:'404-555-4564'},
      {label: '1', value:'404-123-1234'}, 
      {label: '2', value:'678-545-4564'}
      ],
    address1: '123 Main St',
    address2: '',
    city: 'Gainsville',
    state: 'GA',
    zip: '30074',
    notes: [  ]

  },
  {
    title: 'Mr.',
    firstName: 'Mark',
    middleName: 'A',
    lastName: 'Williams',
    suffix: '',
    firmName: 'Williams and Tarker',
    emails: [
      {label: '0', value:'mark@williamstarker.com'}
      ],
    phones: [
      {label: '0', value: '404-555-4564'},
      {label: '2', value:'404-123-1234'},
      {label: '1', value: '678-545-4564'}
      ],
    address: '321 Side St',
    city: 'Atlanta',
    state: 'GA',
    zip: '30072',
    notes: [  ]
  }
],
  
caseDataOneRecord = [
{
  "created": "123456", 
  "trash": false,
  "caseId":  "2014-099", //csi case #
  "generalInfo": {
    "caseName": "Mikes Test Case",
    "crashNum": "C000123456",
    "injuries": "0",
    "fatalities": "0",
    "totalVehicles": "2",
    "noAccidentReport": true,
    "clientType": "plantiff",
    "attorney": "536de25e4e120f40291836da",
    "isCriminal": false
  },
  "crashInfo": {
    "reportingAgency": "APD",
    "officerName": "Smith Johnson",
    "crashDateTime": "Wed May 15 2002 01:00:31 GMT-0400 (Eastern Daylight Time)",
    "road": "Hwy 19",
    "intersection": "285",
    "city": "Atlanta",
    "county": "Fulton",
    "state": "GA",
    "cityLimits": true,

  },
  "participants": {
    "vehicles": ['5352aaa2bd308dfc1d6b2a22', '5352ab408f8d03d03061ba2c'],
    "pedestrians": []
  },
  "billing": {
    "billAttorney": false,
    "firmName": "Jacques Trucking and Donuts",
    "firstName": "Jacques",
    "lastName": "Cousteau",
    "address1": "952 Washington Ave.",
    "address2": "Apartment #55",
    "city": "Walla Walla",
    "state": "WA",
    "zip": "90027",
    "emails": [
      {label: '1', value:'Jacques@Cousteau.com'}
      ],
    "phones": [
      {label: '2', value:'888-565-4364'},
      {label: '0', value:'545-323-1634'}, 
      {label: '1', value:'223-985-4804'}
      ],
  },
  "tasks": [],
  "notes": []
}],

caseData = [];

/**
 * Populate database with sample application data
 */

//Clear old cases, then add things in
var populateCollectionCases = function() {
    CaseFile.find({}).remove(function() {
  
      CaseFile.create( caseDataOneRecord, function(err) {
          console.log('finished populating cases');
      });


    });
},

populateCollectionUsers = function() {
  User.find({}).remove(function() {
    User.create({
      provider: 'local',
      name: 'Michael',
      email: 'wmfeuvrel@gmail.com',
      password: 'test'
    },{
      provider: 'local',
      name: 'Daniel',
      email: 'daniel.nelson711@gmail.com',
      password: 'test'
    },{
      provider: 'local',
      name: 'Jeff',
      email: 'jkidd@collisionspecialistsinc.com',
      password: 'test'
    },{
      provider: 'local',
      name: 'Angie',
      email: 'acoker@collisionspecialistsinc.com',
      password: 'test'
    },
     function() {
        console.log('finished populating users');
      }
    );
  });
},

/*
Should no longer run this.. or need to..
populateCollectionNotes = function() {
  Note.find({}).remove(function() {
    Note.create(notes,
     function() {
        console.log('finished populating notes');
      }
    );
  });
},
*/
populateCollectionVehicles = function() {
  Vehicle.find({}).remove(function() {
    Vehicle.create(vehicles,
     function() {
        console.log('finished populating vehicles');
      }
    );
  });
},

populateCollectionPassengers = function() {
  Passenger.find({}).remove(function() {
    Passenger.create(passengers,
     function() {
        console.log('finished populating passengers');
      }
    );
  });
},

populateCollectionAttorneys = function() {
  Attorney.find({}).remove(function() {
    Attorney.create(attorneys,
     function() {
        console.log('finished populating attorneys');
      }
    );
  });
};


//populateCollectionCases();
//populateCollectionUsers();
//populateCollectionAttorneys();
//populateCollectionPassengers();
//populateCollectionVehicles();



