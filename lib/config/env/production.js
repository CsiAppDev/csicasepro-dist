'use strict';

module.exports = {
  env: 'production',
  mongo: {
    uri: process.env.MONGOLAB_URI ||
         process.env.MONGOHQ_URL ||
         'mongodb://l0ngta115a11y:t!m31ngblu3t1g3r@ds033579.mongolab.com:33579/csicasepro'
         //'mongodb://c$t3rw0ll1ng:m1dn1ghtb!!rn@ds045089.mongolab.com:45089/csicaseprov201'
         //'mongodb://inmobiliscp201:dr1ve$afe@ds045089.mongolab.com:45089/csicaseprov201' 
         //'mongodb://heroku_app22265332:dr1ve$afe@ds033429.mongolab.com:33429/heroku_app25522397' 
  }
};