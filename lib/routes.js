'use strict';

var index = require('./controllers'),
    caseFile = require('./controllers/caseFile'),
    oldCaseFile = require('./controllers/oldCaseFile'),
    attorney = require('./controllers/attorney'),
    vehicle = require('./controllers/vehicle'),
    passenger = require('./controllers/passenger'),
    pedestrian = require('./controllers/pedestrian'),
    log = require('./controllers/log'),
    note = require('./controllers/note'),
    agency = require('./controllers/agency'),
    task = require('./controllers/task'),
    users = require('./controllers/users'),
    search = require('./controllers/search'),
    session = require('./controllers/session');
    

var middleware = require('./middleware');

/**
 * Application routes
 *
 *
 */


module.exports = function(app) {

  app.get('/api/search', middleware.auth, search.search);

  // Server API Routes
  app.post('/api/case-file', middleware.auth, caseFile.create);
  app.put('/api/case-file/:id', middleware.auth,  caseFile.saveFile);
  app.get('/api/case-file', middleware.auth, caseFile.queryCases);
  app.get('/api/case-file/:id', middleware.auth, caseFile.showCase);
  app.del('/api/case-file/:id', middleware.auth, caseFile.removeCaseFile);

  app.get('/api/case-files', middleware.auth, caseFile.getCases);

  app.get('/api/case-count', middleware.auth, caseFile.getCaseCount);

  app.post('/api/case-filev1', middleware.auth, oldCaseFile.create);
  app.put('/api/case-filev1/:id', middleware.auth,  oldCaseFile.saveFile);
  app.get('/api/case-filev1', middleware.auth, oldCaseFile.queryCases);
  app.get('/api/case-filev1/:id', middleware.auth, oldCaseFile.showCase);
  app.del('/api/case-filev1/:id', middleware.auth, oldCaseFile.removeOldCaseFile);
  
  app.post('/api/attorney', middleware.auth, attorney.create);
  app.put('/api/attorney/:id', middleware.auth,  attorney.save);
  app.get('/api/attorney', middleware.auth, attorney.query);
  app.get('/api/attorney/:id', middleware.auth, attorney.show);
  app.del('/api/attorney/:id', middleware.auth, attorney.remove);

  app.post('/api/vehicle', middleware.auth, vehicle.create);
  app.put('/api/vehicle/:id', middleware.auth,  vehicle.save);
  app.get('/api/vehicle', middleware.auth, vehicle.query);
  app.get('/api/vehicle/:id', middleware.auth, vehicle.show);
  app.del('/api/vehicle/:id', middleware.auth, vehicle.remove);

  app.post('/api/passenger', middleware.auth, passenger.create);
  app.put('/api/passenger/:id', middleware.auth,  passenger.save);
  app.get('/api/passenger', middleware.auth, passenger.query);
  app.get('/api/passenger/:id', middleware.auth, passenger.show);
  app.del('/api/passenger/:id', middleware.auth, passenger.remove);

  app.post('/api/pedestrian', middleware.auth, pedestrian.create);
  app.put('/api/pedestrian/:id', middleware.auth,  pedestrian.save);
  app.get('/api/pedestrian', middleware.auth, pedestrian.query);
  app.get('/api/pedestrian/:id', middleware.auth, pedestrian.show);
  app.del('/api/pedestrian/:id', middleware.auth, pedestrian.remove);

  app.post('/api/task', middleware.auth, task.create);
  app.put('/api/task/:id', middleware.auth,  task.save);
  app.get('/api/task', middleware.auth, task.query);
  app.get('/api/task/:id', middleware.auth, task.show);
  app.del('/api/task/:id', middleware.auth, task.remove);

  app.post('/api/log', middleware.auth, log.create);
  app.put('/api/log/:id', middleware.auth,  log.save);
  app.get('/api/log', middleware.auth, log.query);
  app.get('/api/log/:id', middleware.auth, log.show);
  app.del('/api/log/:id', middleware.auth, log.remove);

  app.post('/api/note', middleware.auth, note.create);
  app.put('/api/note/:id', middleware.auth,  note.save);
  app.get('/api/note', middleware.auth, note.query);
  app.get('/api/note/:id', middleware.auth, note.show);
  app.del('/api/note/:id', middleware.auth, note.remove);

  app.post('/api/agency', middleware.auth, agency.create);
  app.put('/api/agency/:id', middleware.auth,  agency.save);
  app.get('/api/agency', middleware.auth, agency.query);
  app.get('/api/agency/:id', middleware.auth, agency.show);
  app.del('/api/agency/:id', middleware.auth, agency.remove);

  app.post('/api/users', middleware.auth, users.create);
  app.put('/api/users', middleware.auth, users.changePassword);
  app.get('/api/users', middleware.auth, users.query);
  app.get('/api/users/me', middleware.auth, users.me);
  app.get('/api/users/:id', middleware.auth, users.show);

  app.post('/api/session', session.login);
  app.del('/api/session', session.logout);

  // All other routes to use Angular routing in app/scripts/app.js
  
  app.get('/login', index.index);
  app.get('/partials/message', index.partials);
  app.get('/partials/login', index.partials);
  app.get('/partials/header', index.partials);
  app.get('/partials/footer', index.partials);
  app.get('/partials/cases', middleware.auth, index.partials);
  app.get('/partials/*', middleware.auth, index.partials);
  app.get('/*', middleware.auth, index.index);
};